﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace Veterinaria
{
    public partial class PrincipalPage : Form
    {
        public PrincipalPage()
        {
            InitializeComponent();
            timer1.Enabled = true;
        }
        private void PrincipalPage_Load(object sender, EventArgs e)
        {
            conexion cRaza = new conexion();
            cRaza.llenarCboRaza(CboRaza);
            conexion cClientes = new conexion();
            cClientes.llenarCboClientes(CboCliente);
            conexion cServicios = new conexion();
            cServicios.llenarCboServicios(CboServicio);
            conexion cColores = new conexion();
            cColores.llenarCboColores(cboColor);
        }
        private void CboServicio_SelectedIndexChanged(object sender, EventArgs e)
        {
            OleDbConnection cnn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=Veterinaria.accdb");
            string cadcon = "SELECT Costo FROM Servicios Where Servicio = '" + CboServicio.Text + "'";
            OleDbCommand comando = new OleDbCommand(cadcon, cnn);
            cnn.Open();
            OleDbDataReader LectorDatos = comando.ExecuteReader();
            if (LectorDatos.Read() == true)
            {
                LblCosto.Text = "$" + LectorDatos["Costo"].ToString();
            }
            else
            {
                LblCosto.Text = "Error de datos.";
            }
            cnn.Close();
        }
        private void nuevoClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddCliente FrmaddCliente = new AddCliente();
            FrmaddCliente.Show();
            this.Hide();
        }
        private void agregarServicioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddServicios FrmaddServicios = new AddServicios();
            FrmaddServicios.Show();
            this.Hide();
        }
        private void agregarRazaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddRaza FrmaddRaza = new AddRaza();
            FrmaddRaza.Show();
            this.Hide();
        }
        private void agregarUsuarioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddUsuario FrmaddUsuario = new AddUsuario();
            FrmaddUsuario.Show();
            this.Hide();
        }
        private void agregarColorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddColores FrmaddColores = new AddColores();
            FrmaddColores.Show();
            this.Hide();
        }
        private void buscarRegistrosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Busqueda FrmBusqueda = new Busqueda();
            FrmBusqueda.Show();
            this.Hide();
        }
        private void modificarColorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ModColores FrmModColores = new ModColores();
            FrmModColores.Show();
            this.Hide();
        }
        private void modificarAtributosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ModUsuario FrmModUsuario = new ModUsuario();
            FrmModUsuario.Show();
            this.Hide();
        }
        private void modificarRazaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ModRaza FrmModRaza = new ModRaza();
            FrmModRaza.Show();
            this.Hide();
        }
        private void corteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Corte FrmCorte = new Corte();
            FrmCorte.Show();
            this.Hide();
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            lblFecha.Text = DateTime.Now.ToString();

        }
        private void BtnCerrar_Click(object sender, EventArgs e)
        {
            DialogResult rs = MessageBox.Show("Desea Salir", "Salir", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
            if (rs == DialogResult.Yes)
            {
                Application.Exit();
            }
        }
        private void CboRaza_SelectedIndexChanged(object sender, EventArgs e)
        {
            OleDbConnection cnn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=Veterinaria.accdb");
            cnn.Open();
            OleDbDataAdapter dap = new OleDbDataAdapter("SELECT Id,SubRaza FROM Mascota WHERE Raza = '" + CboRaza.Text + "'", cnn);
            DataTable tbl = new DataTable();
            dap.Fill(tbl);
            cboSubRaza.DataSource = tbl;
            cboSubRaza.DisplayMember = "SubRaza";
            cboSubRaza.ValueMember = "Id";
        }
        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                //1. Crear objeto conexión...                   
                OleDbConnection cnn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=Veterinaria.accdb");
                //2. Conectar a base de datos...
                cnn.Open();
                //3. Query(Validar datos)...
                String Guardar = "INSERT INTO Registro(Veterinario, Cliente, NombreMascota, Edad, Servicio, Costo, Fecha, Peso, Raza, SubRaza, Color, Observaciones) values (@Veterinario, @Cliente, @NombreMascota, @Edad, @Servicio, @Costo, @Fecha, @Peso, @Raza, @SubRaza, @Color, @Observaciones)";
                OleDbCommand cmd = new OleDbCommand(Guardar, cnn);
                cmd.Parameters.AddWithValue("@Veterinario", LblVeterinario.Text);
                cmd.Parameters.AddWithValue("@idCliente", CboCliente.Text);
                cmd.Parameters.AddWithValue("@NombreMascota", txtBoxNombreMascota.Text);
                cmd.Parameters.AddWithValue("@Edad", txtBoxEdad.Text);
                cmd.Parameters.AddWithValue("@Servicio", CboServicio.Text);
                cmd.Parameters.AddWithValue("@Costo", LblCosto.Text);
                cmd.Parameters.AddWithValue("@Fecha", DateTime.Now.ToString("dd/MM/yyyy"));
                cmd.Parameters.AddWithValue("@Peso", TxtBoxPeso.Text);
                cmd.Parameters.AddWithValue("@Raza", CboRaza.Text);
                cmd.Parameters.AddWithValue("@SubRaza", cboSubRaza.Text);
                cmd.Parameters.AddWithValue("@Color", cboColor.Text);
                cmd.Parameters.AddWithValue("@Observaciones", txtBoxObservaciones.Text);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Registro Guardado Exitosamente.", "Exito.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cnn.Close();
            }
            catch (OleDbException ex)
            {
                MessageBox.Show("Error de Concurrencia:\n" + ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void modificarServicioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ModServicio FrmServicio = new ModServicio();
            FrmServicio.Show();
            this.Hide();
        }
        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult rs = MessageBox.Show("Desea Salir", "Salir", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
            if (rs == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void ayudaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("Manual de usuario.docx");
        }
    }
}
