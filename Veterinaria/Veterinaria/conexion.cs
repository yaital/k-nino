﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;
using System.Windows.Forms;

namespace Veterinaria
{
    class conexion
    {
        OleDbConnection cnn;
        OleDbCommand comando;
        OleDbDataReader LectorDatos;  
        public conexion()
        {
            try
            {
                cnn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=Veterinaria.accdb");
                cnn.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void llenarCboRaza(ComboBox cb)
        {
            try
            {
                string consulta = "SELECT DISTINCT Raza FROM Mascota";
                comando = new OleDbCommand(consulta, cnn);
                LectorDatos = comando.ExecuteReader();
                while (LectorDatos.Read())
                {
                    cb.Items.Add(LectorDatos["Raza"].ToString());
                }
                LectorDatos.Close();
                cnn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void llenarCboClientes(ComboBox cb)
        {
            try
            {
                string consulta = "Select Id,Nombre FROM Clientes";
                comando = new OleDbCommand(consulta, cnn);
                LectorDatos = comando.ExecuteReader();
                while (LectorDatos.Read())
                {
                    cb.Items.Add(LectorDatos["Nombre"].ToString());
                }
                LectorDatos.Close();
                cnn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void llenarCboServicios(ComboBox cb)
        {
            try
            {
                string consulta = "SELECT Id,Servicio FROM Servicios";
                comando = new OleDbCommand(consulta, cnn);
                LectorDatos = comando.ExecuteReader();
                while (LectorDatos.Read())
                {
                    cb.Items.Add(LectorDatos["Servicio"].ToString());
                }
                cb.SelectedItem = 0;
                LectorDatos.Close();
                cnn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void llenarCboColores(ComboBox cb)
        {
            try
            {
                string consulta = "SELECT Id,Color FROM Color";
                comando = new OleDbCommand(consulta, cnn);
                LectorDatos = comando.ExecuteReader();
                while (LectorDatos.Read())
                {
                    cb.Items.Add(LectorDatos["Color"].ToString());
                }
                cb.SelectedItem = 0;
                LectorDatos.Close();
                cnn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void llenarcbousuarios(ComboBox cb)
        {
            try
            {
                string consulta = "SELECT Usuario FROM Veterinarios";
                comando = new OleDbCommand(consulta, cnn);
                LectorDatos = comando.ExecuteReader();
                while (LectorDatos.Read())
                {
                    cb.Items.Add(LectorDatos["Usuario"].ToString());
                }
                cb.SelectedItem = 0;
                LectorDatos.Close();
                cnn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
