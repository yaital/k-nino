﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace Veterinaria
{
    public partial class ModServicio : Form
    {
        public ModServicio()
        {
            InitializeComponent();
        }

        private void ModCliente_Load(object sender, EventArgs e)
        {
            conexion cServicio = new conexion();
            cServicio.llenarCboServicios(cboServicio);
        }

        private void cboCliente_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                OleDbConnection cnn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=Veterinaria.accdb");
                String cadcon = "SELECT Servicio,Costo FROM Servicios Where Servicio = '" + cboServicio.Text + "'";
                OleDbCommand comando = new OleDbCommand(cadcon, cnn);
                cnn.Open();
                OleDbDataReader LectorDatos = comando.ExecuteReader();
                if (LectorDatos.Read() == true)
                {
                    TxtBoxNombre.Text = LectorDatos["Servicio"].ToString();
                    TxtBoxCosto.Text = LectorDatos["Costo"].ToString();
                }
                else
                {
                    TxtBoxNombre.Text = "Error de datos.";
                }
                cnn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            PrincipalPage FrmPrincipal = new PrincipalPage();
            FrmPrincipal.Show();
            this.Close();
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                OleDbConnection cnn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=Veterinaria.accdb");
                cnn.Open();
                String Modificar = "UPDATE Servicios SET Servicio = '" + TxtBoxNombre.Text + "', Costo = '" + TxtBoxCosto.Text + "' WHERE Servicio = @Servicio";
                OleDbCommand comando = new OleDbCommand(Modificar, cnn);
                comando.Parameters.AddWithValue("@Servicio", cboServicio.Text);
                comando.ExecuteNonQuery();
                MessageBox.Show("Servicio Modificado Satisfactoriamente.", "Exito", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cnn.Close();
                PrincipalPage FrmPrincipal = new PrincipalPage();
                this.Close();
                FrmPrincipal.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            } 
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                OleDbConnection cnn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=Veterinaria.accdb");
                cnn.Open();
                String Borrar = "DELETE FROM Servicios WHERE Servicio = '" + TxtBoxNombre.Text + "'";
                OleDbCommand comando = new OleDbCommand(Borrar, cnn);
                MessageBox.Show("Servicio Eliminado Satisfactoriamente.", "Exito", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cnn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            } 
        }
    }
}
