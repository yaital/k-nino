﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace Veterinaria
{
    public partial class ModUsuario : Form
    {
        public ModUsuario()
        {
            InitializeComponent();
        }

        private void ModUsuario_Load(object sender, EventArgs e)
        {
            conexion cUsuario = new conexion();
            cUsuario.llenarcbousuarios(cboUsuario);
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            PrincipalPage FrmPrincipal = new PrincipalPage();
            FrmPrincipal.Show();
            this.Close();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                OleDbConnection cnn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=Veterinaria.accdb");
                cnn.Open();
                String Borrar = "DELETE FROM Veterinarios WHERE Usuario = '" + cboUsuario.Text + "'";
                OleDbCommand comando = new OleDbCommand(Borrar, cnn);
                MessageBox.Show("Servicio Eliminado Satisfactoriamente.", "Exito", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cnn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            } 
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                OleDbConnection cnn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=Veterinaria.accdb");
                cnn.Open();
                String Modificar = "UPDATE Veterinarios SET Usuario = '" + txtBoxUsuario.Text + "', Contraseña = '" + txtBoxRContraseña.Text + "', Nombre = '" + txtBoxNombre.Text + "', Direccion = '" + txtBoxDireccion.Text + "', Telefono = '" + txtBoxTelefono.Text + "', Atributos = '" + CmbPrivilegios.Text + "' WHERE Usuario = @Usuario";
                OleDbCommand comando = new OleDbCommand(Modificar, cnn);
                comando.Parameters.AddWithValue("@Usuario", cboUsuario.Text);
                comando.ExecuteNonQuery();
                MessageBox.Show("Usuario Modificado Satisfactoriamente.", "Exito", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cnn.Close();
                PrincipalPage FrmPrincipal = new PrincipalPage();
                this.Close();
                FrmPrincipal.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            } 
        }

        private void cboUsuario_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                OleDbConnection cnn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=Veterinaria.accdb");
                String cadcon = "SELECT Usuario,Contraseña,Nombre,Direccion,Telefono,Atributos FROM Veterinarios Where Usuario = '" + cboUsuario.Text + "'";
                OleDbCommand comando = new OleDbCommand(cadcon, cnn);
                cnn.Open();
                OleDbDataReader LectorDatos = comando.ExecuteReader();
                if (LectorDatos.Read() == true)
                {
                    txtBoxNombre.Text = LectorDatos["Nombre"].ToString();
                    txtBoxDireccion.Text = LectorDatos["Direccion"].ToString();
                    txtBoxTelefono.Text = LectorDatos["Telefono"].ToString();
                    txtBoxUsuario.Text = LectorDatos["Usuario"].ToString();
                    txtBoxContraseña.Text = LectorDatos["Contraseña"].ToString();
                    CmbPrivilegios.Text = LectorDatos["Atributos"].ToString();
                }
                else
                {
                    txtBoxNombre.Text = "Error de datos.";
                }
                cnn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
