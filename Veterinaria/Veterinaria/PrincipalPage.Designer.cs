﻿namespace Veterinaria
{
    partial class PrincipalPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PrincipalPage));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.BtnGuardar = new System.Windows.Forms.Button();
            this.BtnCerrar = new System.Windows.Forms.Button();
            this.txtBoxObservaciones = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtBoxEdad = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBoxNombreMascota = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.opcionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buscarRegistrosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuevoClienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gerenteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.serviciosCostosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agregarServicioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarServicioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.coloresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agregarColorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarColorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usuariosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agregarUsuarioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarAtributosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.razasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agregarRazaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarRazaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.corteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ayudaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label4 = new System.Windows.Forms.Label();
            this.CboCliente = new System.Windows.Forms.ComboBox();
            this.CboRaza = new System.Windows.Forms.ComboBox();
            this.CboServicio = new System.Windows.Forms.ComboBox();
            this.LblCosto = new System.Windows.Forms.Label();
            this.cboSubRaza = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cboColor = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.lblFecha = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.TxtBoxPeso = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.LblVeterinario = new System.Windows.Forms.Label();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(16, 41);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(243, 58);
            this.pictureBox1.TabIndex = 41;
            this.pictureBox1.TabStop = false;
            // 
            // BtnGuardar
            // 
            this.BtnGuardar.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnGuardar.Location = new System.Drawing.Point(721, 461);
            this.BtnGuardar.Name = "BtnGuardar";
            this.BtnGuardar.Size = new System.Drawing.Size(97, 33);
            this.BtnGuardar.TabIndex = 40;
            this.BtnGuardar.Text = "Aceptar";
            this.BtnGuardar.UseVisualStyleBackColor = true;
            this.BtnGuardar.Click += new System.EventHandler(this.BtnGuardar_Click);
            // 
            // BtnCerrar
            // 
            this.BtnCerrar.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCerrar.Location = new System.Drawing.Point(12, 461);
            this.BtnCerrar.Name = "BtnCerrar";
            this.BtnCerrar.Size = new System.Drawing.Size(97, 33);
            this.BtnCerrar.TabIndex = 39;
            this.BtnCerrar.Text = "Cerrar";
            this.BtnCerrar.UseVisualStyleBackColor = true;
            this.BtnCerrar.Click += new System.EventHandler(this.BtnCerrar_Click);
            // 
            // txtBoxObservaciones
            // 
            this.txtBoxObservaciones.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBoxObservaciones.Location = new System.Drawing.Point(12, 309);
            this.txtBoxObservaciones.Multiline = true;
            this.txtBoxObservaciones.Name = "txtBoxObservaciones";
            this.txtBoxObservaciones.Size = new System.Drawing.Size(806, 141);
            this.txtBoxObservaciones.TabIndex = 38;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(9, 289);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(118, 17);
            this.label7.TabIndex = 37;
            this.label7.Text = "Observaciones";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(485, 468);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(148, 17);
            this.label6.TabIndex = 35;
            this.label6.Text = "Costo por Servicio:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(380, 226);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(157, 17);
            this.label5.TabIndex = 34;
            this.label5.Text = "Color de la Mascota.";
            // 
            // txtBoxEdad
            // 
            this.txtBoxEdad.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBoxEdad.Location = new System.Drawing.Point(12, 247);
            this.txtBoxEdad.Name = "txtBoxEdad";
            this.txtBoxEdad.Size = new System.Drawing.Size(149, 25);
            this.txtBoxEdad.TabIndex = 33;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(276, 169);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(154, 17);
            this.label3.TabIndex = 31;
            this.label3.Text = "Raza de la Mascota.";
            // 
            // txtBoxNombreMascota
            // 
            this.txtBoxNombreMascota.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBoxNombreMascota.Location = new System.Drawing.Point(12, 190);
            this.txtBoxNombreMascota.Name = "txtBoxNombreMascota";
            this.txtBoxNombreMascota.Size = new System.Drawing.Size(240, 25);
            this.txtBoxNombreMascota.TabIndex = 30;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 169);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(175, 17);
            this.label2.TabIndex = 29;
            this.label2.Text = "Nombre de la Mascota.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 116);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(152, 17);
            this.label1.TabIndex = 28;
            this.label1.Text = "Nombre del Cliente.";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.opcionesToolStripMenuItem,
            this.buscarRegistrosToolStripMenuItem,
            this.nuevoClienteToolStripMenuItem,
            this.gerenteToolStripMenuItem,
            this.ayudaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(838, 24);
            this.menuStrip1.TabIndex = 27;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // opcionesToolStripMenuItem
            // 
            this.opcionesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.salirToolStripMenuItem});
            this.opcionesToolStripMenuItem.Name = "opcionesToolStripMenuItem";
            this.opcionesToolStripMenuItem.Size = new System.Drawing.Size(69, 20);
            this.opcionesToolStripMenuItem.Text = "Opciones";
            // 
            // buscarRegistrosToolStripMenuItem
            // 
            this.buscarRegistrosToolStripMenuItem.Name = "buscarRegistrosToolStripMenuItem";
            this.buscarRegistrosToolStripMenuItem.Size = new System.Drawing.Size(105, 20);
            this.buscarRegistrosToolStripMenuItem.Text = "Buscar Registros";
            this.buscarRegistrosToolStripMenuItem.Click += new System.EventHandler(this.buscarRegistrosToolStripMenuItem_Click);
            // 
            // nuevoClienteToolStripMenuItem
            // 
            this.nuevoClienteToolStripMenuItem.Name = "nuevoClienteToolStripMenuItem";
            this.nuevoClienteToolStripMenuItem.Size = new System.Drawing.Size(94, 20);
            this.nuevoClienteToolStripMenuItem.Text = "Nuevo Cliente";
            this.nuevoClienteToolStripMenuItem.Click += new System.EventHandler(this.nuevoClienteToolStripMenuItem_Click);
            // 
            // gerenteToolStripMenuItem
            // 
            this.gerenteToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.serviciosCostosToolStripMenuItem,
            this.coloresToolStripMenuItem,
            this.usuariosToolStripMenuItem,
            this.razasToolStripMenuItem,
            this.corteToolStripMenuItem});
            this.gerenteToolStripMenuItem.Name = "gerenteToolStripMenuItem";
            this.gerenteToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.gerenteToolStripMenuItem.Text = "Gerente";
            // 
            // serviciosCostosToolStripMenuItem
            // 
            this.serviciosCostosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.agregarServicioToolStripMenuItem,
            this.modificarServicioToolStripMenuItem});
            this.serviciosCostosToolStripMenuItem.Name = "serviciosCostosToolStripMenuItem";
            this.serviciosCostosToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.serviciosCostosToolStripMenuItem.Text = "Servicios/Costos";
            // 
            // agregarServicioToolStripMenuItem
            // 
            this.agregarServicioToolStripMenuItem.Name = "agregarServicioToolStripMenuItem";
            this.agregarServicioToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.agregarServicioToolStripMenuItem.Text = "Agregar Servicio";
            this.agregarServicioToolStripMenuItem.Click += new System.EventHandler(this.agregarServicioToolStripMenuItem_Click);
            // 
            // modificarServicioToolStripMenuItem
            // 
            this.modificarServicioToolStripMenuItem.Name = "modificarServicioToolStripMenuItem";
            this.modificarServicioToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.modificarServicioToolStripMenuItem.Text = "Modificar Servicio";
            this.modificarServicioToolStripMenuItem.Click += new System.EventHandler(this.modificarServicioToolStripMenuItem_Click);
            // 
            // coloresToolStripMenuItem
            // 
            this.coloresToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.agregarColorToolStripMenuItem,
            this.modificarColorToolStripMenuItem});
            this.coloresToolStripMenuItem.Name = "coloresToolStripMenuItem";
            this.coloresToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.coloresToolStripMenuItem.Text = "Colores";
            // 
            // agregarColorToolStripMenuItem
            // 
            this.agregarColorToolStripMenuItem.Name = "agregarColorToolStripMenuItem";
            this.agregarColorToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.agregarColorToolStripMenuItem.Text = "Agregar Color";
            this.agregarColorToolStripMenuItem.Click += new System.EventHandler(this.agregarColorToolStripMenuItem_Click);
            // 
            // modificarColorToolStripMenuItem
            // 
            this.modificarColorToolStripMenuItem.Name = "modificarColorToolStripMenuItem";
            this.modificarColorToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.modificarColorToolStripMenuItem.Text = "Modificar Color";
            this.modificarColorToolStripMenuItem.Click += new System.EventHandler(this.modificarColorToolStripMenuItem_Click);
            // 
            // usuariosToolStripMenuItem
            // 
            this.usuariosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.agregarUsuarioToolStripMenuItem,
            this.modificarAtributosToolStripMenuItem});
            this.usuariosToolStripMenuItem.Name = "usuariosToolStripMenuItem";
            this.usuariosToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.usuariosToolStripMenuItem.Text = "Usuarios";
            // 
            // agregarUsuarioToolStripMenuItem
            // 
            this.agregarUsuarioToolStripMenuItem.Name = "agregarUsuarioToolStripMenuItem";
            this.agregarUsuarioToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.agregarUsuarioToolStripMenuItem.Text = "Agregar Usuario";
            this.agregarUsuarioToolStripMenuItem.Click += new System.EventHandler(this.agregarUsuarioToolStripMenuItem_Click);
            // 
            // modificarAtributosToolStripMenuItem
            // 
            this.modificarAtributosToolStripMenuItem.Name = "modificarAtributosToolStripMenuItem";
            this.modificarAtributosToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.modificarAtributosToolStripMenuItem.Text = "Modificar Atributos";
            this.modificarAtributosToolStripMenuItem.Click += new System.EventHandler(this.modificarAtributosToolStripMenuItem_Click);
            // 
            // razasToolStripMenuItem
            // 
            this.razasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.agregarRazaToolStripMenuItem,
            this.modificarRazaToolStripMenuItem});
            this.razasToolStripMenuItem.Name = "razasToolStripMenuItem";
            this.razasToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.razasToolStripMenuItem.Text = "Razas";
            // 
            // agregarRazaToolStripMenuItem
            // 
            this.agregarRazaToolStripMenuItem.Name = "agregarRazaToolStripMenuItem";
            this.agregarRazaToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.agregarRazaToolStripMenuItem.Text = "Agregar Raza";
            this.agregarRazaToolStripMenuItem.Click += new System.EventHandler(this.agregarRazaToolStripMenuItem_Click);
            // 
            // modificarRazaToolStripMenuItem
            // 
            this.modificarRazaToolStripMenuItem.Name = "modificarRazaToolStripMenuItem";
            this.modificarRazaToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.modificarRazaToolStripMenuItem.Text = "Modificar Raza";
            this.modificarRazaToolStripMenuItem.Click += new System.EventHandler(this.modificarRazaToolStripMenuItem_Click);
            // 
            // corteToolStripMenuItem
            // 
            this.corteToolStripMenuItem.Name = "corteToolStripMenuItem";
            this.corteToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.corteToolStripMenuItem.Text = "Corte";
            this.corteToolStripMenuItem.Click += new System.EventHandler(this.corteToolStripMenuItem_Click);
            // 
            // ayudaToolStripMenuItem
            // 
            this.ayudaToolStripMenuItem.Name = "ayudaToolStripMenuItem";
            this.ayudaToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.ayudaToolStripMenuItem.Text = "Ayuda";
            this.ayudaToolStripMenuItem.Click += new System.EventHandler(this.ayudaToolStripMenuItem_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(9, 226);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(154, 17);
            this.label4.TabIndex = 32;
            this.label4.Text = "Edad de la Mascota.";
            // 
            // CboCliente
            // 
            this.CboCliente.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboCliente.FormattingEnabled = true;
            this.CboCliente.Location = new System.Drawing.Point(12, 141);
            this.CboCliente.Name = "CboCliente";
            this.CboCliente.Size = new System.Drawing.Size(806, 25);
            this.CboCliente.TabIndex = 47;
            // 
            // CboRaza
            // 
            this.CboRaza.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboRaza.FormattingEnabled = true;
            this.CboRaza.Location = new System.Drawing.Point(279, 190);
            this.CboRaza.Name = "CboRaza";
            this.CboRaza.Size = new System.Drawing.Size(258, 25);
            this.CboRaza.TabIndex = 48;
            this.CboRaza.SelectedIndexChanged += new System.EventHandler(this.CboRaza_SelectedIndexChanged);
            // 
            // CboServicio
            // 
            this.CboServicio.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboServicio.FormattingEnabled = true;
            this.CboServicio.Location = new System.Drawing.Point(560, 247);
            this.CboServicio.Name = "CboServicio";
            this.CboServicio.Size = new System.Drawing.Size(258, 25);
            this.CboServicio.TabIndex = 49;
            this.CboServicio.SelectedIndexChanged += new System.EventHandler(this.CboServicio_SelectedIndexChanged);
            // 
            // LblCosto
            // 
            this.LblCosto.AutoSize = true;
            this.LblCosto.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCosto.Location = new System.Drawing.Point(639, 468);
            this.LblCosto.Name = "LblCosto";
            this.LblCosto.Size = new System.Drawing.Size(67, 17);
            this.LblCosto.TabIndex = 50;
            this.LblCosto.Text = "$000.00";
            // 
            // cboSubRaza
            // 
            this.cboSubRaza.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboSubRaza.FormattingEnabled = true;
            this.cboSubRaza.Location = new System.Drawing.Point(570, 190);
            this.cboSubRaza.Name = "cboSubRaza";
            this.cboSubRaza.Size = new System.Drawing.Size(248, 25);
            this.cboSubRaza.TabIndex = 52;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(567, 169);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(187, 17);
            this.label8.TabIndex = 51;
            this.label8.Text = "Sub-Raza de la Mascota.";
            // 
            // cboColor
            // 
            this.cboColor.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboColor.FormattingEnabled = true;
            this.cboColor.Location = new System.Drawing.Point(383, 247);
            this.cboColor.Name = "cboColor";
            this.cboColor.Size = new System.Drawing.Size(154, 25);
            this.cboColor.TabIndex = 54;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(555, 226);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(151, 17);
            this.label9.TabIndex = 53;
            this.label9.Text = "Servicio a Realizar.";
            // 
            // lblFecha
            // 
            this.lblFecha.AutoSize = true;
            this.lblFecha.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFecha.Location = new System.Drawing.Point(485, 82);
            this.lblFecha.Name = "lblFecha";
            this.lblFecha.Size = new System.Drawing.Size(55, 15);
            this.lblFecha.TabIndex = 55;
            this.lblFecha.Text = "label10";
            this.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // TxtBoxPeso
            // 
            this.TxtBoxPeso.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBoxPeso.Location = new System.Drawing.Point(205, 247);
            this.TxtBoxPeso.Name = "TxtBoxPeso";
            this.TxtBoxPeso.Size = new System.Drawing.Size(149, 25);
            this.TxtBoxPeso.TabIndex = 57;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(202, 226);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(153, 17);
            this.label10.TabIndex = 56;
            this.label10.Text = "Peso de la Mascota.";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(485, 41);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(83, 15);
            this.label11.TabIndex = 59;
            this.label11.Text = "Bienvenido,";
            // 
            // LblVeterinario
            // 
            this.LblVeterinario.AutoSize = true;
            this.LblVeterinario.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblVeterinario.Location = new System.Drawing.Point(567, 41);
            this.LblVeterinario.Name = "LblVeterinario";
            this.LblVeterinario.Size = new System.Drawing.Size(0, 15);
            this.LblVeterinario.TabIndex = 58;
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.salirToolStripMenuItem.Text = "Salir";
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // PrincipalPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(838, 504);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.LblVeterinario);
            this.Controls.Add(this.TxtBoxPeso);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.lblFecha);
            this.Controls.Add(this.cboColor);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.cboSubRaza);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.LblCosto);
            this.Controls.Add(this.CboServicio);
            this.Controls.Add(this.CboRaza);
            this.Controls.Add(this.CboCliente);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.BtnGuardar);
            this.Controls.Add(this.BtnCerrar);
            this.Controls.Add(this.txtBoxObservaciones);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtBoxEdad);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtBoxNombreMascota);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.label4);
            this.MaximumSize = new System.Drawing.Size(854, 543);
            this.MinimumSize = new System.Drawing.Size(854, 543);
            this.Name = "PrincipalPage";
            this.Text = "K-NINO";
            this.Load += new System.EventHandler(this.PrincipalPage_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button BtnGuardar;
        private System.Windows.Forms.Button BtnCerrar;
        private System.Windows.Forms.TextBox txtBoxObservaciones;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtBoxEdad;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBoxNombreMascota;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem opcionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuevoClienteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gerenteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem serviciosCostosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agregarServicioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modificarServicioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem coloresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agregarColorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem usuariosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agregarUsuarioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modificarAtributosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem razasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agregarRazaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modificarRazaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ayudaToolStripMenuItem;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox CboCliente;
        private System.Windows.Forms.ComboBox CboRaza;
        private System.Windows.Forms.ComboBox CboServicio;
        private System.Windows.Forms.Label LblCosto;
        private System.Windows.Forms.ComboBox cboSubRaza;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cboColor;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ToolStripMenuItem corteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem buscarRegistrosToolStripMenuItem;
        private System.Windows.Forms.Label lblFecha;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TextBox TxtBoxPeso;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ToolStripMenuItem modificarColorToolStripMenuItem;
        public System.Windows.Forms.Label LblVeterinario;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
    }
}