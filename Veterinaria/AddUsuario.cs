﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace Veterinaria
{
    public partial class AddUsuario : Form
    {
        public AddUsuario()
        {
            InitializeComponent();
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                OleDbConnection conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=Veterinaria.accdb");
                conexion.Open();
                string guardar = "insert into Veterinarios(Usuario, Contraseña, Nombre, Direccion, Telefono, Atributos) values (@Usuario, @Contraseña, @Nombre, @Direccion, @Telefono, @Atributos)";
                if (txtBoxContraseña.Text == txtBoxRContraseña.Text)
                {
                    OleDbCommand comando = new OleDbCommand(guardar, conexion);
                    comando.Parameters.AddWithValue("@Usuario", txtBoxUsuario.Text);
                    comando.Parameters.AddWithValue("@Contraseña", txtBoxContraseña.Text);
                    comando.Parameters.AddWithValue("@Nombre", txtBoxNombre.Text);
                    comando.Parameters.AddWithValue("@Direccion", txtBoxDireccion.Text);
                    comando.Parameters.AddWithValue("@Telefono", txtBoxTelefono.Text);
                    if (CmbPrivilegios.Text == "Administrador")
                    {
                        CmbPrivilegios.Text = "0";
                    }
                    else
                    {
                        CmbPrivilegios.Text = "1";
                    }
                    comando.Parameters.AddWithValue("@Atributos", CmbPrivilegios.Text);
                    comando.ExecuteNonQuery();
                    MessageBox.Show("Registro Guardado Exitosamente.", "Exito.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    conexion.Close();
                    this.Close();
                    PrincipalPage FrmPrincipal = new PrincipalPage();
                    FrmPrincipal.Show();
                }
                else
                {
                    MessageBox.Show("Contraseña no Coincide", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
            PrincipalPage FrmPrincipal = new PrincipalPage();
            FrmPrincipal.Show();
        }

        private void txtBoxContraseña_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
