﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace Veterinaria
{
    public partial class AddRaza : Form
    {
        public AddRaza()
        {
            InitializeComponent();
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                OleDbConnection conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=Veterinaria.accdb");
                conexion.Open();
                String guardar = "insert into Mascota(Raza, SubRaza) values (@Raza, @SubRaza)";
                OleDbCommand comando = new OleDbCommand(guardar, conexion);
                comando.Parameters.AddWithValue("@Raza", cboRaza.Text);
                comando.Parameters.AddWithValue("@SubRaza", TxtBoxSubRaza.Text);
                comando.ExecuteNonQuery();
                MessageBox.Show("Registro Guardado Exitosamente.", "Exito.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                conexion.Close();
                this.Close();
                PrincipalPage FrmPrincipal = new PrincipalPage();
                FrmPrincipal.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al grabar la raza: " + ex.Message);
            }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
            PrincipalPage FrmPrincipal = new PrincipalPage();
            FrmPrincipal.Show();
        }

        private void AddRaza_Load(object sender, EventArgs e)
        {
            conexion cRaza = new conexion();
            cRaza.llenarCboRaza(cboRaza);
        }
    }
}
