﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace Veterinaria
{
    public partial class AddServicios : Form
    {
        public AddServicios()
        {
            InitializeComponent();
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                OleDbConnection conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=Veterinaria.accdb");
                conexion.Open();
                String Guardar = "insert into Servicios(Servicio, Costo) values (@Servicio,@Costo)";
                OleDbCommand comando = new OleDbCommand(Guardar, conexion);
                comando.Parameters.AddWithValue("@Servicio", txtBoxNombre.Text);
                comando.Parameters.AddWithValue("@Costo", txtBoxCosto.Text);
                comando.ExecuteNonQuery();
                MessageBox.Show("Servicio grabado exitosamente!", "Exito!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                conexion.Close();
                this.Close();
                PrincipalPage FrmPrincipal = new PrincipalPage();
                FrmPrincipal.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show("No fue posible guardar el nuevo servicio: " + ex.Message);
            }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
            PrincipalPage FrmPrincipal = new PrincipalPage();
            FrmPrincipal.Show();
        }
    }
}
