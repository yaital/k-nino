﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace Veterinaria
{
    public partial class ModColores : Form
    {
        public ModColores()
        {
            InitializeComponent();
        }

        private void ModColores_Load(object sender, EventArgs e)
        {
            conexion cColores = new conexion();
            cColores.llenarCboColores(cboColores);
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                OleDbConnection cnn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=Veterinaria.accdb");
                cnn.Open();
                String Borrar = "DELETE FROM Color WHERE Color = '" + cboColores.Text + "'";
                OleDbCommand comando = new OleDbCommand(Borrar, cnn);
                MessageBox.Show("Servicio Eliminado Satisfactoriamente.", "Exito", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cnn.Close();
                PrincipalPage FrmPrincipal = new PrincipalPage();
                this.Close();
                FrmPrincipal.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            } 
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                OleDbConnection cnn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=Veterinaria.accdb");
                cnn.Open();
                String Modificar = "UPDATE Color SET Color = '" + TxtBoxNombre.Text + "' WHERE Color = @Color";
                OleDbCommand comando = new OleDbCommand(Modificar, cnn);
                comando.Parameters.AddWithValue("@Color", cboColores.Text);
                comando.ExecuteNonQuery();
                MessageBox.Show("Servicio Modificado Satisfactoriamente.", "Exito", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cnn.Close();
                PrincipalPage FrmPrincipal = new PrincipalPage();
                this.Close();
                FrmPrincipal.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            } 
        }

        private void cboColores_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                OleDbConnection cnn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=Veterinaria.accdb");
                String cadcon = "SELECT Color FROM Color Where Color = '" + cboColores.Text + "'";
                OleDbCommand comando = new OleDbCommand(cadcon, cnn);
                cnn.Open();
                OleDbDataReader LectorDatos = comando.ExecuteReader();
                if (LectorDatos.Read() == true)
                {
                    TxtBoxNombre.Text = LectorDatos["Color"].ToString();
                }
                else
                {
                    TxtBoxNombre.Text = "Error de datos.";
                }
                cnn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
