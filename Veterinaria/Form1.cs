﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace Veterinaria
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void BtnSesion_Click(object sender, EventArgs e)
        {
            //1. Crear objeto conexión...                   
            OleDbConnection Conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=Veterinaria.accdb");
            //2. Conectar a base de datos...
            Conexion.Open();
            //3. Query(Validar datos)...
            String Consulta = "select Usuario,Contraseña from Veterinarios where Usuario ='" + TxtBoxUsuario.Text + "' and Contraseña ='" + TxtBoxContraseña.Text + "';";
            //4. ejecutar query...
            OleDbCommand Comando = new OleDbCommand(Consulta, Conexion);
            //5.Variable reader...
            OleDbDataReader LectorDatos;
            //6.Ejecutar Query...
            LectorDatos = Comando.ExecuteReader();
            //7.Validar Lector de datos con registro
            Boolean ExistenciaRegistros = LectorDatos.HasRows;
            //8.Validamos entrada del usuario
            if (ExistenciaRegistros)
            {
                MessageBox.Show("Bienvenido " + TxtBoxUsuario.Text, "Usuario Autorizado", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                //Cargar el Formulario principal de nuestra aplicacion
                PrincipalPage FrmPrincipal = new PrincipalPage();
                this.Hide();
                PrincipalPage NombreUsuario = new PrincipalPage();
                NombreUsuario.LblVeterinario.Text = TxtBoxUsuario.Text;
                NombreUsuario.ShowDialog();
            }
            else
            {
                MessageBox.Show("Acceso no valido", "Datos Erroneos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            //9. Cerramos la conexion a la base de datos
            Conexion.Close();
        }

        private void BtnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
