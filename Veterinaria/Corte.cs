﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace Veterinaria
{
    public partial class Corte : Form
    {
        public Corte()
        {
            InitializeComponent();
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            PrincipalPage FrmPrincipal = new PrincipalPage();
            FrmPrincipal.Show();
            this.Close();
        }

        private void dtpFin_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                OleDbConnection cnn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=Veterinaria.accdb");
                cnn.Open();
                String Consulta = "SELECT Veterinario, Cliente, NombreMascota, Edad, Servicio, Costo, Fecha, Peso, Raza, SubRaza, Color, Observaciones FROM Registro WHERE Fecha >= '" + dtpInicio.Text + "' AND Fecha <= '" + dtpFin.Text + "'";
                OleDbDataAdapter daRegistros = new OleDbDataAdapter(Consulta, cnn);
                DataSet dsRegistros = new DataSet();
                daRegistros.Fill(dsRegistros, "Registro");
                dgvCorte.DataSource = dsRegistros;
                dgvCorte.DataMember = "Registro";
                double sumatoria = 0;
                //Método con el que recorreremos todas las filas de nuestro Datagridview
                foreach (DataGridViewRow row in dgvCorte.Rows)
                {
                    //Aquí seleccionaremos la columna que contiene los datos numericos.
                    sumatoria += Convert.ToDouble(row.Cells["Costo"].Value);
                }
                //Por ultimo asignamos el resultado a el texto de nuestro Label
                LblCorte.Text = Convert.ToString(sumatoria);
                cnn.Close();    
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
    }
}
