﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace Veterinaria
{
    public partial class AddCliente : Form
    {
        public AddCliente()
        {
            InitializeComponent();
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                //1. Crear objeto conexión...                   
                OleDbConnection Conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=Veterinaria.accdb");
                //2. Conectar a base de datos...
                Conexion.Open();
                //3. Query(Validar datos)...
                String Guardar = "insert into Clientes(Nombre, Telefono, Direccion) values (@Nombre,@Telefono,@Direccion)";
                OleDbCommand cmd = new OleDbCommand(Guardar, Conexion);
                cmd.Parameters.AddWithValue("@Nombre", TxtBoxNombre.Text);
                cmd.Parameters.AddWithValue("@Telefono", TxtBoxTelefono.Text);
                cmd.Parameters.AddWithValue("@Direccion", TxtBoxDireccion.Text);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Registro Guardado Exitosamente.", "Exito.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Conexion.Close();
                this.Close();
                PrincipalPage FrmPrincipal = new PrincipalPage();
                FrmPrincipal.Show();
            }
            catch (OleDbException ex)
            {
                MessageBox.Show("Error de Concurrencia:\n" + ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
            PrincipalPage FrmPrincipal = new PrincipalPage();
            FrmPrincipal.Show();
        }

        private void AddCliente_Load(object sender, EventArgs e)
        {

        }
    }
}
