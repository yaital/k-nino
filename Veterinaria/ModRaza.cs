﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace Veterinaria
{
    public partial class ModRaza : Form
    {
        public ModRaza()
        {
            InitializeComponent();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                OleDbConnection cnn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=Veterinaria.accdb");
                cnn.Open();
                String Borrar = "DELETE FROM Mascota WHERE SubRaza = '" + CboSubRaza.Text + "'";
                OleDbCommand comando = new OleDbCommand(Borrar, cnn);
                MessageBox.Show("Servicio Eliminado Satisfactoriamente.", "Exito", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cnn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            } 
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            PrincipalPage FrmPrincipal = new PrincipalPage();
            this.Close();
            FrmPrincipal.Show();
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                OleDbConnection cnn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=Veterinaria.accdb");
                cnn.Open();
                String Modificar = "UPDATE Mascota SET Raza = '" + txtboxRaza.Text + "', SubRaza = '" + TxtBoxSubRaza.Text + "' WHERE SubRaza = @SubRaza";
                OleDbCommand comando = new OleDbCommand(Modificar, cnn);
                comando.Parameters.AddWithValue("@SubRaza", CboSubRaza.Text);
                comando.ExecuteNonQuery();
                MessageBox.Show("Servicio Modificado Satisfactoriamente.", "Exito", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cnn.Close();
                PrincipalPage FrmPrincipal = new PrincipalPage();
                this.Close();
                FrmPrincipal.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            } 
        }

        private void ModRaza_Load(object sender, EventArgs e)
        {
            conexion cRaza = new conexion();
            cRaza.llenarCboRaza(cboRaza);
        }

        private void cboRaza_SelectedIndexChanged(object sender, EventArgs e)
        {
            OleDbConnection cnn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=Veterinaria.accdb");
            cnn.Open();
            OleDbDataAdapter dap = new OleDbDataAdapter("SELECT Id,SubRaza FROM Mascota WHERE Raza = '" + cboRaza.Text + "'", cnn);
            DataTable tbl = new DataTable();
            dap.Fill(tbl);
            CboSubRaza.DataSource = tbl;
            CboSubRaza.DisplayMember = "SubRaza";
            CboSubRaza.ValueMember = "Id";
        }

        private void CboSubRaza_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                OleDbConnection cnn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=Veterinaria.accdb");
                String cadcon = "SELECT Raza,SubRaza FROM Mascota Where SubRaza = '" + CboSubRaza.Text + "'";
                OleDbCommand comando = new OleDbCommand(cadcon, cnn);
                cnn.Open();
                OleDbDataReader LectorDatos = comando.ExecuteReader();
                if (LectorDatos.Read() == true)
                {
                    txtboxRaza.Text = LectorDatos["Raza"].ToString();
                    TxtBoxSubRaza.Text = LectorDatos["SubRaza"].ToString();
                }
                else
                {
                    txtboxRaza.Text = "Error de datos.";
                }
                cnn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
