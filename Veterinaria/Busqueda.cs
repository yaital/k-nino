﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace Veterinaria
{
    public partial class Busqueda : Form
    {
        public Busqueda()
        {
            InitializeComponent();
        }

        private void BtnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                OleDbConnection cnn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=Veterinaria.accdb");
                cnn.Open();
                String Consulta = "SELECT Veterinario, Cliente, NombreMascota, Edad, Servicio, Costo, Fecha, Peso, Raza, SubRaza, Color, Observaciones FROM Registro WHERE " + cboBuscar.Text + " LIKE '%" + TxtBoxBusqueda.Text + "%'";
                OleDbDataAdapter daRegistros = new OleDbDataAdapter(Consulta, cnn);
                DataSet dsRegistros = new DataSet();
                daRegistros.Fill(dsRegistros, "Registro");
                dgvRegistro.DataSource = dsRegistros;
                dgvRegistro.DataMember = "Registro";
                cnn.Close();
            }
            catch (Exception ex)
            {
                
                MessageBox.Show(ex.Message);
            }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            PrincipalPage FrmPrincipalPage = new PrincipalPage();
            FrmPrincipalPage.Show();
            this.Close();
        }

        private void TxtBoxBusqueda_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                try
                {
                    OleDbConnection cnn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=Veterinaria.accdb");
                    cnn.Open();
                    String Consulta = "SELECT Veterinario, Cliente, NombreMascota, Edad, Servicio, Costo, Fecha, Peso, Raza, SubRaza, Color, Observaciones FROM Registro WHERE " + cboBuscar.Text + " LIKE '%" + TxtBoxBusqueda.Text + "%'";
                    OleDbDataAdapter daRegistros = new OleDbDataAdapter(Consulta, cnn);
                    DataSet dsRegistros = new DataSet();
                    daRegistros.Fill(dsRegistros, "Registro");
                    dgvRegistro.DataSource = dsRegistros;
                    dgvRegistro.DataMember = "Registro";
                    cnn.Close();
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }
    }
}
